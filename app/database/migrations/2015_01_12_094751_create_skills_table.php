<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('skill', 50);
			$table->timestamps();
		});
		Schema::create('user_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('skill_id')->references('id')->on('skills');
		});
		Schema::create('project_skills', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('project_id')->references('id')->on('projects');
			$table->integer('skill_id')->references('id')->on('skills');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('skills');
		Schema::drop('user_skills');
		Schema::drop('project_skills');
	}

}
