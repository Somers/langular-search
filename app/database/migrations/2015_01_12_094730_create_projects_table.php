<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('name', 32);
			$table->dateTime('bidding_deadline');
			$table->float('budget');
			$table->string('location', 32);
			$table->longText('description');

			$table->integer('user_id')->references('id')->on('users');
			$table->integer('service_id')->nullable()->references('id')->on('services');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('projects');
	}

}
