<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');

			$table->string('username', 32);

			$table->string('organisation', 50)->nullable();
			$table->string('forename', 25)->nullable();
			$table->string('surname', 50)->nullable();
			$table->string('email', 50);
			
			$table->boolean('email_verified')->default(0);
			$table->string('verification_code', 60)->nullable();
			$table->string('password', 60);
			$table->text('security');

			$table->string('address_1', 32)->nullable();
			$table->string('address_2', 32)->nullable();
			$table->string('city', 32)->nullable();
			$table->string('state', 32)->nullable();
			$table->string('zipcode', 10)->nullable();
			$table->string('country', 32)->nullable();

			$table->date('founding_date')->nullable();
			$table->date('date_of_birth')->nullable();

			$table->boolean('is_professional')->default(false);
			$table->boolean('is_individual')->default(false);
			$table->boolean('is_organisation')->default(false);

			$table->longText('biography')->nullable();
			$table->longText('personal_statement')->nullable();
			$table->longText('interests')->nullable();

			$table->integer('account_type')->default(0);

			$table->timestamps();
		});

		Schema::create('security_questions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('question_number');
			$table->string('question');
		});

		Schema::create('user_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type', 16);
			$table->integer('user_id')->references('id')->on('users');
			$table->integer('target_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('security_questions');
		Schema::drop('user_users');
	}

}
