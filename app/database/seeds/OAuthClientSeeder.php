<?php

/*
|--------------------------------------------------------------------------
| Seed client table
|--------------------------------------------------------------------------
|
| This seeder adds a client to the 'oauth_clients' table for testing.
|
*/

class OAuthClientSeeder extends Seeder
{
	public function run()
	{
		DB::table('users')->insert(array(
			'username' => 'BerryM24',
			'organisation' => 'ExtremeBru',
			'forename' => 'Mathew',
			'surname' => 'Berry',
			'email' => 'matthew.berry@myfuturesbright.co.uk',
			'email_verified' => 1,
			'password' => Hash::make('password'),
			'security' => '{"2": "'.Hash::make('answer1').'", "3": "'.Hash::make('answer2').'", "5": "'.Hash::make('answer3').'"}',
			'address_1' => 'Fenchurch Ave',
			'address_2' => '8',
			'city' => 'Manchester',
			'state' => 'Lancashire',
			'zipcode' => 'M40 2XF',
			'country' => 'England',
			'founding_date' => date('Y-m-d H:i:s', time()),
			'date_of_birth' => date('Y-m-d H:i:s', time() - 504576000 ),
			'is_professional' => 1,
			'is_individual' => 1,
			'is_organisation' => 1,
			'biography' => 'Nope.',
			'personal_statement' => 'Nope.',
			'interests' => 'Nope.',
			'account_type' => 5,
			'created_at' => date('Y-m-d H:i:s', time()),
			'updated_at' => date('Y-m-d H:i:s', time())
		));

		$id = 'professionals_nigeria';
		$secret = 'secret';

		DB::table('oauth_clients')->insert(array(
			'id' => $id,
			'secret' => $secret,
			'name' => 'Global Client',
			'created_at' => date('Y-m-d H:i:s', time()),
			'updated_at' => date('Y-m-d H:i:s', time())
		));
	}
}