<?php

class SkillTableSeeder extends DatabaseSeeder {

    public function run()
    {
        Skill::create(['skill' => 'Web']);
        Skill::create(['skill' => 'Mobile']);
        Skill::create(['skill' => 'Software']);
        Skill::create(['skill' => 'Baking']);
        Skill::create(['skill' => 'Plumbing']);
    }

}
