<?php

class Proposal extends Eloquent
{

	protected $table = 'Proposals';

	public function users()
	{		
		return $this->belongsTo('User');
	}

	public function projects()
	{		
		return $this->belongsTo('Project');
	}

	public function milestone()
	{
		return $this->hasMany('Milestone');
	}

	public function documents()
	{
		return $this->hasMany('Document');
	}

}