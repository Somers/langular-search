<?php

class Project extends Eloquent
{

	protected $table = 'Projects';

	public function users() 
	{		
		return $this->belongsTo('User');
	}

	public function addons()
	{
		return $this->belongsToMany('Addon');
	}

	public function proposal()
	{
		return $this->hasMany('Proposal');
	}

	public function skills()
	{
		return $this->belongsToMany('Skill');
	}

	public function documents()
	{
		return $this->hasMany('Document');
	}

}