<?php

class Education extends Eloquent
{

	protected $table = 'Education';

	public function users()
	{		
		return $this->belongsTo('User');
	}
}