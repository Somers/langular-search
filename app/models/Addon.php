<?php

class Addon extends Eloquent
{

	protected $table = 'Addons';

	public function projects()
	{
		return $this->belongsToMany('Project');
	}

}