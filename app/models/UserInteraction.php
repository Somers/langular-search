<?php

class UserInteraction extends Eloquent{

	protected $table = 'user_users';

	public function users(){		

		return $this->morphedByMany('User');
	}
}