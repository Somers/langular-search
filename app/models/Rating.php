<?php

class Rating extends Eloquent
{

	protected $table = 'Ratings';

	public function users()
	{		
		return $this->belongsTo('User');
	}

	public function services()
	{		
		return $this->belongsTo('Service');
	}
}