<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface 
{

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token', 'verification_code');

	/**
	 * The attributes protected from mass assignment
	 * @var array
	 */
	protected $guarded = ['id'];

	/**
	 * Defines the relationship between users and skills
	 * @return (Relationship) Skills
	 */
	public function skills()
	{
		return $this->belongsToMany('Skill', 'user_skills', 'user_id', 'skill_id');
	}

	public function educations()
	{
		return $this->hasMany('Education');
	}

	public function experiences()
	{
		return $this->hasMany('Experience');
	}

	public function userInterations()
	{
		return $this->morphToMany('UserInteraction','userInteraction');
	}

	public function services()
	{
		return $this->hasMany('Service','service');
	}

	public function ratings()
	{
		return $this->hasMany('Rating');
	}

	public function images()
	{
		return $this->hasMany('image');
	}

	public function projects()
	{
		return $this->hasMany('Project');
	}

	public function proposals()
	{
		return $this->hasMany('Proposal');
	}

	public function payments()
	{
		return $this->hasMany('Payment');
	}

	public function groups()
	{
		return $this->belongsToMany('Group');
	}

	/**
	 * Finds a user from a given username
	 * @param $username
	 * @return bool
	 */
	public function findByUsername($username)
	{
		if(!is_null($user = static::whereUsername($username)->first())) {
			return $user;
		} else {
			return false;
		}
	}

	/**
	 * Finds a user from a given email address
	 * @param $email
	 * @return bool
	 */
	public function findByEmail($email)
	{
		if(!is_null($user = static::whereEmail($email)->first())) {
			return $user;
		} else {
			return false;
		}
	}

	public function getRememberToken()
	{
		return null;
	}

	public function setRememberToken($value)
	{
		//
	}

	public function getRememberTokenName()
	{
		return null;
	}
}
