<?php

class Group extends Eloquent
{

	protected $table = 'Groups';

	public function users()
	{
		return $this->belongsToMany('Users');
	}

}