<?php

class Document extends Eloquent
{

	protected $table = 'Documents';

	public function proposals()
	{

		$this->belongsTo('Proposal');
	}

	public function project()
	{
		$this->belongsTo('Projects');
	}

	public function milestones()
	{
		$this->belongsTo('Milestones');
	}


}