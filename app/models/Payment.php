<?php

class Payment extends Eloquent
{

	protected $table = 'Payments';

	public function milstones()
	{

		$this->belongsTo('Milstone');
	}

	public function users()
	{
		$this->belongsTo('User');
	}


}