<?php

class Experience extends Eloquent
{

	protected $table = 'Experience';

	public function users()
	{		
		return $this->belongsTo('User');
	}
}