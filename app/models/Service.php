<?php

class Service extends Eloquent
{

	protected $table = 'Services';

	public function users()
	{		
		return $this->belongsTo('User');
	}

	public function ratings()
	{
		return $this->hasMany('Rating');
	}

	public function images()
	{
		return $this->morphToMany('Image','image' );
	}
}