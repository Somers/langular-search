<?php

class Milestone extends Eloquent
{

	protected $table = 'Milestones';

	public function projects()
	{
		return $this->belongsTo('Proposal');
	}

	public function payments()
	{
		return $this->hasMany('Payment');
	}

	public function documents(){
		return $this->hasMany('Document');
	}

}