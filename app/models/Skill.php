<?php

class Skill extends Eloquent
{

	/**
	 * The database table used by the model.
	 * @var string
	 */
	protected $table = 'skills';

	/**
	 * Defines the relationship between skills and users
	 * @return (Relationship) Users
	 */
	function users()
	{
		return $this->belongsToMany('User', 'user_skills', 'skill_id', 'user_id');
	}

	function projects()
	{
		return $this->belongsToMany('Project');
	}

}
